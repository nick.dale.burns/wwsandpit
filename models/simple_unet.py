"""
Here we create a very simple, very small UNet-styled convnet. We've kept this small, as it is just for testing
weightwatcher. But still nice to have something similar to our domain.
"""
import tensorflow as tf

class SmallUnet:

    def __init__(self, input_shape, output_dimension, depth):

        self.input_layer = tf.keras.layers.Input(input_shape)
        self.output_dimension = output_dimension
        self._depth = depth

    def embedding_block(self, input_layer, name=""):
        """Embedding blocks will simply be a 2D MLP-styled block:
              (input) -> conv2d -> conv2d -> BN -> activation
        """
        x = input_layer
        for i in range(2):
            x = tf.keras.layers.Conv2D(32, (3, 3), strides=1, padding='same', name=f"{name}_embedding_{i}")(x)
        x = tf.keras.layers.BatchNormalization()(x)
        x = tf.keras.layers.Activation('relu')(x)

        return x


    def downsample_block(self, input_layer, name=""):
        """
        Downsampling blocks will be a simple strided convolution, followed by channel mixing
        """
        x = tf.keras.layers.Conv2D(64, (5, 5), strides=2, padding='same', name=f"{name}_downsample")(input_layer)
        x = tf.keras.layers.Conv2D(32, (1, 1), strides=1, padding='same', name=f"{name}_downsample_mixing")(x)
        return x


    def upsample_block(self, input_layer, name=""):
        """
        Upsampling layers will be bilinear upsampling, followed by channel mixing.
        """
        x = tf.keras.layers.UpSampling2D((2, 2), interpolation='bilinear', name=f"{name}_upsample")(input_layer)
        x = tf.keras.layers.Conv2D(32, (1, 1), strides=1, padding='same', name=f"{name}_upsample_mixing")(x)
        return x

    def unet(self):
        """
        Our unet will have self._depth downsampling blocks in the decoder and self._depth upsampling blocks in the decoder.
        Super simple.
        """

        # Encoder
        x = self.input_layer 
        for i in range(self._depth):
            x = self.embedding_block(x, name=f"enc_layer_{i}")
            x = self.downsample_block(x, name=f"enc_layer_{i}")

        # Decoder
        for i in range(self._depth):
            x = self.embedding_block(x, name=f"dec_layer_{i}")
            x = self.upsample_block(x, name=f"dec_layer_{i}")

        # Linear Prediction head
        x = tf.keras.layers.Conv2D(self.output_dimension, (3, 3), strides=1, padding='same', name='unet_prediction_head')(x)

        return x 

    def model(self):
        return tf.keras.models.Model(inputs = self.input_layer, outputs = self.unet())


if __name__ == '__main__':

    unet = SmallUnet(
        input_shape=(224, 224, 3),
        output_dimension=8,
        depth=4
    )
    print(unet.model().summary())