import warnings
warnings.simplefilter(action="ignore", category=RuntimeWarning)

import json
import pandas as pd
import weightwatcher as ww

from models.simple_unet import SmallUnet

if __name__ == '__main__':

    unet = SmallUnet(
        input_shape=(112, 112, 3),
        output_dimension=8,
        depth=3
    )
    watcher = ww.WeightWatcher(model=unet.model())
    details = watcher.analyze(mp_fit=True, randomize=True, vectors=False)

    with open("results/ww_summary.json", 'w') as outfile:
        outfile.write(json.dumps(watcher.get_summary(details)))

    details.to_csv("results/ww_details.csv", index=False)