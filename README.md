# WWSandpit

Very simple test bed for WeightWatcher. We create a toy UNet and run this through WW. 

To setup:
```
$ python3 -n venv .venv
$ source .venv/bin/activate
(venv) $ pip install -r requirements.txt
(venv) $ python run_weightwatcher.py
```

Weightwatcher's summary output and the full details are saved to the results folder.